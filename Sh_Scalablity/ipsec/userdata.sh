#!/bin/bash

export VPN_SERVER_IP='90.90.90.2'
export VPN_IPSEC_PSK='123456789'
#export VPN_USER='sandeep'
#export VPN_PASSWORD='sandeep'


#Configure ip and route
ifconfig eth0 "$MY_IP/16"
#route add default gw 10.10.1.1
#echo "nameserver 10.10.1.1" > /etc/resolv.conf


#sleep 30
#Configure IPSEC

cat > /etc/ipsec.conf <<EOF
# ipsec.conf - strongSwan IPsec configuration file

# basic configuration

config setup
  # strictcrlpolicy=yes
  # uniqueids = no

# Add connections here.

# Sample VPN connections

conn %default
  ikelifetime=60m
  keylife=20m
  rekeymargin=3m
  keyingtries=1
  keyexchange=ikev1
  authby=secret
  ike=aes128-sha1-modp2048!
  esp=aes128-sha1-modp2048!

conn test
  left=%any
  right=$VPN_SERVER_IP
  leftsubnet=10.10.5.0/32
  rightsubnet=192.168.70.0/24
  keyexchange=ikev1
  auto=route
  aggressive=no
  authby=secret
  ike=aes128-sha1-modp1024!
  ikelifetime=3600
  lifetime=1800
  esp=aes128-sha1!
  type=tunnel


conn myvpn
  left=%any
  right=90.90.90.2
  rightsubnet=192.168.51.0/24
  keyexchange=ikev1
  auto=route
  aggressive=no
  authby=secret
  ike=aes128-sha1-modp1024!
  ikelifetime=3600
  lifetime=1800
  esp=aes128-sha1!
  type=tunnel

EOF

cat > /root/ping.sh <<EOF

ping 192.168.51.2 

EOF


#Configure IPSEC Secret
cat > /etc/ipsec.secrets <<EOF
: PSK "$VPN_IPSEC_PSK"
EOF

chmod 600 /etc/ipsec.secrets

cat > /root/id_rsa <<EOF
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAzHBtAdKvG5YxpCLqnj20avqlptg3VA5+0NCbroWKGl90Ok/v
/ERKky1O3NHxM1gciHnfvx3TB8jbVhok8c/I4yuGpptt7PVdXMwPHJLNCEY80NNA
WFFRhU1VM2NeaFQtLe2w9TuwOH3g/CcuTvZ+JgeHArU6aGXze1P1EN1rtPr1VC96
N8xzdIozZiAhVOV9ntcfmztb5589VRTz0y7jqPduRpQ6nVd6HA/36PIDUzwM+iPc
xMvmN5/6kWSu3x5oZUi6MeCpsCiYtGvdtWf3peZ+Ac6AgSuhiFefQcDNQXR1yxK8
U7zaxDqebTQM2ZJ3ta177r8s8dUviEMtByIkoQIDAQABAoIBABlGFNPS3f8FFGH8
RxEFA9bt4ai8tipYGGlsQLLUo3wbLN1AoKSJNMb5JPS7p5v3Wp+soXKZF0uxExyT
4tlaHLWo3d6e/Hn842vjdkLMCzjopgAS/bNtaiyWoN/XewZe0a8J4YSPunthZH0H
wvvUEUhWSeSVzwC1CeLHQFazaOtYml1EGWOfsUDapIbups0RZ1teffAaN4mXks5e
DcJcZwAo0x2DM/IK1ZE0UOdBJBRwmYjGg1pLmPTbWVUnHfa6UVd80vkaKAYcrh9J
L5YfUILzlGXc236hDeMSTvrQ0/qTM4j1OZlDiIOIwTCa5LcqSxlVK1NvHfI7C6J5
yfFGwPkCgYEA9thc+O1RvmgphD37gZS2ZXnocZVVv9C60HhqdzTQ9IJbmlKk3rH+
fYuRiERjWa1XvwNZn6o/df17XKRvlYiAkZQPhgMngwFPiFgJQP+ozTwfcDuaGaLz
Z85+wwR1n236QGbcsLTWEW2eKYHlI7dRds/1xXl6oczk8kd4jeOlZeMCgYEA1AVx
3Dx6rfQNGQVJKKNskaR2kcGkZ5S9fCgn7GVsfTGcKrU8eSOQ3Nb1jczPhWNDedcw
/Q/3Eh3ShYRLImFJmVXc7PcDbpbwL4RIW+T80sEb8bvNGXimKs7idtcdVorERM5K
UCvJ+hcv7eSrkzAPxE5LsWIrSRBORs+Am6EYcqsCgYAnlvX6Wp2kqzIVapd+5eoH
BnFYXjodhI2hx95Tdh4ZSRsF+kFFEkkdu5fAwi8o/9s14ISsTfLVqqzxY9IdqoHs
V1HSDYl1wm0OEgiR6y8Yk477QiPp/q+JM54zCvEOe+YKFDVLBgXbT7hxMcsdWlZv
dtUVMfGQJ7uEfCACqcMqPwKBgCHI2FLaxEWxzOB+yiCSofs3NLmXHKZwuhztnSb7
JmVwoUzg4kj/VNcL3ccBqtO7dIYWtzSr9ZfKbsBdy0coL30ihHiyB7b4kD9SGeGi
xHKk2NoVLeJwKjJKEyYSsxcWN54rLQZHZADY7wc+V+S1VN3YCBG07ebPlVmNORYT
fnzPAoGAWrpw/5lFTdO+5vQhhPqkxqjUDOXj2S5x6uslLaqLYJNukO2vmaC4bLX6
PV9RnFVA/Jz8p4QtpGjaEj9MbhgFP9wOif0lxbSJubI6PjCOfYE7ge8sG7O9mn5r
C3Vi6hXNJN2q0pZU50Nm6Lu5xewDXaLp1MiYZ1zWkjknG/PqcHc=
-----END RSA PRIVATE KEY-----
EOF

chmod 400 /root/id_rsa
#Configure xl2tpd

#cat > /etc/xl2tpd/xl2tpd.conf <<EOF
#[lac myvpn]
#lns = $VPN_SERVER_IP
#ppp debug = yes
#pppoptfile = /etc/ppp/options.l2tpd.client
#length bit = yes
#EOF

#cat > /etc/ppp/options.l2tpd.client <<EOF
#ipcp-accept-local
#ipcp-accept-remote
#refuse-eap
#require-chap
#noccp
#noauth
#mtu 1280
#mru 1280
#noipdefault
#defaultroute
#usepeerdns
#connect-delay 5000
#name $VPN_USER
#password $VPN_PASSWORD
#EOF


#chmod 600 /etc/ppp/options.l2tpd.client

#Create xl2tpd control file
#mkdir -p /var/run/xl2tpd
#touch /var/run/xl2tpd/l2tp-control


#Resrart IPSEC and xl2tp services
service strongswan restart
#service xl2tpd restart

sleep 10

#Start Session
ipsec up myvpn
#echo "c myvpn" > /var/run/xl2tpd/l2tp-control

#sleep 10
#route add $VPN_SERVER_IP gw 10.10.1.1
#route add default dev ppp0
ping 192.168.51.2 -c 2
#sleep 10
#wget -qO- http://ipv4.icanhazip.com; echo
