#!/bin/bash

START_SEQ=16
END_SEQ=25
LXC_CONTAINER_BASE_NAME="ipsec-gw-client-"
IP_ADDR_START="192.168.99."
IP_LAN_NET_START="192.168."
IP_LAN_NET_END=".1"
LAN_NET_END=".0"
for i in $(seq $START_SEQ $END_SEQ);
	do
	IP_ADDR_END="8"
	IP_ADDR="$IP_ADDR_START$START_SEQ"

	IP_LAN="$IP_LAN_NET_START$START_SEQ$IP_LAN_NET_END"
	LAN_NET="$IP_LAN_NET_START$START_SEQ$LAN_NET_END"
	LXC_CONTAINER_NAME="$LXC_CONTAINER_BASE_NAME$i"
	echo $LAN_NET
	echo $IP_ADDR
	echo $LXC_CONTAINER_NAME
	echo $IP_LAN
#	lxc exec $LXC_CONTAINER_NAME -- bash -c "killall -9 ipsec"
#	lxc exec $LXC_CONTAINER_NAME -- bash -c "killall -9 ping"
#	lxc exec $LXC_CONTAINER_NAME -- bash -c "killall -9 ssh"
	cat individual-userdata_new.sh | lxc exec $LXC_CONTAINER_NAME --env MY_IP=$IP_ADDR --env IP_LAN=$IP_LAN --env CON_NO=$i --env LAN_NET=$LAN_NET bash
	lxc exec $LXC_CONTAINER_NAME -- nohup bash -c "ping 192.168.1.1 &"
	START_SEQ=`expr $START_SEQ + 1`

	sleep 2
 	done

