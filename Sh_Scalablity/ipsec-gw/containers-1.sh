
#!/bin/bash

LXC_CONTAINER_BASE_NAME="ipsec-gw-client-"
LXC_IMAGE_NAME="tf1-ipsec-gw-client"
START_SEQ=203
END_SEQ=208
for i in $(seq $START_SEQ $END_SEQ);
	do
	  LXC_CONTAINER_NAME="$LXC_CONTAINER_BASE_NAME$i"
	  echo $LXC_CONTAINER_NAME
          lxc launch $LXC_IMAGE_NAME  $LXC_CONTAINER_NAME
          lxc profile add $LXC_CONTAINER_NAME tf1-ipsec-gw-client
	  START_SEQ=`expr $START_SEQ + 1`
	done

