#!/bin/bash

NO_OF_CONTAINERS=11
LXC_CONTAINER_BASE_NAME="openvpn-client-"
IP_ADDR_START="2.2.2."
IP_ADDR_SUB=10
USER_NAME_BASE="openvpnuser"
PASSWORD_BASE="Welcome123"
for i in $(seq $IP_ADDR_SUB $NO_OF_CONTAINERS);
	do
	OPENVPN_USER_NAME="$USER_NAME_BASE$i"
	OPENVPN_PASSWORD="$PASSWORD_BASE"
	IP_ADDR="$IP_ADDR_START$IP_ADDR_SUB"
	LXC_CONTAINER_NAME="$LXC_CONTAINER_BASE_NAME$i"
	echo $IP_ADDR
	echo $LXC_CONTAINER_NAME
	lxc exec $LXC_CONTAINER_NAME -- bash -c "killall -9 openvpn"
	lxc exec $LXC_CONTAINER_NAME -- bash -c "killall -9 ping"
	lxc exec $LXC_CONTAINER_NAME -- bash -c "killall -9 ssh"
	cat userdata.sh | lxc exec $LXC_CONTAINER_NAME --env MY_IP=$IP_ADDR --env USERNAME=$OPENVPN_USER_NAME --env PASSWORD=$OPENVPN_PASSWORD bash
	lxc exec $LXC_CONTAINER_NAME -- nohup bash -c "ping 192.168.10.2 &"
#        lxc exec $LXC_CONTAINER_NAME -- nohup bash -c "ssh -o ServerAliveInterval=30 -o ServerAliveCountMax=120 -o StrictHostKeyChecking=no -i /root/id_rsa -f -N root@192.168.12.10"
	IP_ADDR_SUB=`expr $IP_ADDR_SUB + 1`
	sleep 5
 	done

