sed -n '/###/p' $0 >> client_config.ovpn
sed -n '/grep/!p' client_config.ovpn > configuration.ovpn
sed -n '/sed/!p' configuration.ovpn > client_config.ovpn
sed 's/### //' client_config.ovpn > openvpn_client_config.ovpn
sudo rm -f configuration.ovpn
sudo rm -f client_config.ovpn
sudo apt-get install openvpn
if [ $? -ne 0 ]
then
sudo yum install openvpn
fi
sudo openvpn --config openvpn_client_config.ovpn
### 
### remote 2.2.2.2
### dev tun1
### 
### 
### client
### port 1194
### proto udp
### cipher AES-128-CBC
### auth SHA1
### auth-user-pass
### <ca>
### -----BEGIN CERTIFICATE-----
### MIIDKDCCAhCgAwIBAgIJAOOxbK1XJ/98MA0GCSqGSIb3DQEBCwUAMBIxEDAOBgNV
### BAMMB051Y2xpYXMwHhcNMjEwNDI3MDI1ODAyWhcNMzEwNDI1MDI1ODAyWjASMRAw
### DgYDVQQDDAdOdWNsaWFzMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA
### w8KRzSt/OhdX5gFfyJ38j9qiD3C/lnAONrHK3WjHvAYlgafLtZaK5KvlC7ReBggK
### /ivibp+nfOHggnt0FvI3WlLc/+gDy4c9afEYEyjyaCZPczFgaa0RuPBsnphP1KZ/
### XqVYu4P870lCTHR9FJrX5qe1U1BCasN8skJ7Zc3DYFGszxObyYXOxhxUagqiGQ6e
### DoWRABqNYSBDo+YDePDU/uwNclzSZtCFK7u1zK1b1yVnBRW+wb92nLXnwHIWmIfV
### OZvdBlN2LpDQp/+0yVJAR6BfiPBqih3WA0cpLeQvs4C4t7NEzslGXRPin0Bzkw4G
### y/MdBZPH9CmleTKshvUnPwIDAQABo4GAMH4wHQYDVR0OBBYEFHmu4Jn3PM5QLIng
### vVlkEY2is9scMEIGA1UdIwQ7MDmAFHmu4Jn3PM5QLIngvVlkEY2is9scoRakFDAS
### MRAwDgYDVQQDDAdOdWNsaWFzggkA47FsrVcn/3wwDAYDVR0TBAUwAwEB/zALBgNV
### HQ8EBAMCAQYwDQYJKoZIhvcNAQELBQADggEBADDwwjQVxhPVRUdjgHp7F6PwZ99w
### ciIW7nX1eDAjUl1e46LvnpL211jaHexJuI5BUCgqvW89e8Rj6jrCEbXR5WUnprAH
### 734ZpmWRy2Pf7VlfLlE/aybfe36oZloTBy/RxonkJ8F8BnI7jvb9gs0zJ8kj8FaU
### VdyFy6d0kF8TBocbWFIZaacXbhA/xxsp2CM05WktR1cOHJ+L6vPtmLX8Tj5yJfLl
### qYwIQ14+B8NdJVzr4i4LHgBk933y349NSm002TsJMW+LpIW8r6bTNi5ZMDy8yzZ7
### 3eXco10F3dfEIgjp6b6XhYn9Wwl9H/FH8iNAImOvzp6VhezEFlhNK1ympwU=
### -----END CERTIFICATE-----
### 
### </ca>
### <cert>
### -----BEGIN CERTIFICATE-----
### MIIC/jCCAeYCFDYkQI0CYGHTldxya45UkqFaH5AJMA0GCSqGSIb3DQEBCwUAMBIx
### EDAOBgNVBAMMB051Y2xpYXMwHhcNMjEwNTA0MTEzODQ4WhcNMjIwNTA0MTEzODQ4
### WjBlMQswCQYDVQQGEwJJTjEOMAwGA1UECAwFSW5kaWExDDAKBgNVBAcMA0h5ZDEU
### MBIGA1UECgwLRGxpbmssIEluYy4xCzAJBgNVBAsMAklUMRUwEwYDVQQDDAxvcGVu
### dnBudXNlcjEwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCcU4L5QPZl
### w9EzuyP7Kcrt3fHwEw/NRCXaidmRtUOb+rCnyfJIlGZxb89+W6iOvldJ6D/tKqrc
### pCcOj5mcKregW4PW93sQEDXsYXlhjCkUyp5RhBYmq3+fWdAgjVL7TuhgfjqNWOcn
### zp4B2r2DvlQgY/h4e5qZBsAu0K1ZYE5mLcNyqb06mVpbTu1zmCWJtih3N04GNxRT
### JWxz9GZNPdRq1j1r/MTIb9UWuzbIYxBhz9gkT8rIYCMBa1K0i3gzEhAeehozb/O6
### jai2SAn2E/caY+zljcqoe+dLzpaLOezm0H/2k2gW0223lmXTurFQLzHTsB7PAqY+
### f3rN43epXpfRAgMBAAEwDQYJKoZIhvcNAQELBQADggEBACd1mwZjDHySHec8Cfky
### k7by4Y/193bXtg5TcQ0DkLtaocgVztTnKxxAADfulJmq4o2Jp9Mt5G2hmvSd5pT7
### dLsnO2pQR/Uu8H0/R8Vl+4nEQAOLQd7XKSndZW6JoZ6nygKRfQptaMqsKzwuxAYL
### xcluaswYncounUHkTRE3rsmx+KFzCu05RYEu9T+5Dq7QwJyryh08KESK84/dSD8d
### mLGRCOrEzJpw0BQEfu4cftR8ak2m+oMKZAoln8xihBDB49kxIXmgCFk8qP98qPYO
### JBcf+mWE7eUBfauv7q8e2NAhGUpKfvJDMRUp1bUXAaZTqilefuqDigEdFeHpl1Bm
### sFw=
### -----END CERTIFICATE-----
### 
### </cert>
### <key>
### -----BEGIN RSA PRIVATE KEY-----
### MIIEowIBAAKCAQEAnFOC+UD2ZcPRM7sj+ynK7d3x8BMPzUQl2onZkbVDm/qwp8ny
### SJRmcW/Pfluojr5XSeg/7Sqq3KQnDo+ZnCq3oFuD1vd7EBA17GF5YYwpFMqeUYQW
### Jqt/n1nQII1S+07oYH46jVjnJ86eAdq9g75UIGP4eHuamQbALtCtWWBOZi3Dcqm9
### OplaW07tc5glibYodzdOBjcUUyVsc/RmTT3UatY9a/zEyG/VFrs2yGMQYc/YJE/K
### yGAjAWtStIt4MxIQHnoaM2/zuo2otkgJ9hP3GmPs5Y3KqHvnS86Wizns5tB/9pNo
### FtNtt5Zl07qxUC8x07AezwKmPn96zeN3qV6X0QIDAQABAoIBAHHVkjtFJefG5xEf
### hj0AGuiJxQLILqqPpGz2Eru63ie4LmmQceCOR+b0v4jI3/5J9rA38k0W0+LlmhdN
### VFNW++5GlcO05SU2WL3v4P66nLOqNA0x/RtdqQp10UvS/YlI1YhmguaoxXLET2UU
### TwqfTw1jHu1FPLeQJeQxSvoZWh2mtABdxMr2bful36mmx3JcLie/Tc3rqJHtMuBd
### RFql/fxHmO3s72n0MmyPso/9Mm3jRmCpF4jIfKcj6fgZ8wWnjrDLqPPPJudgksWd
### HKBvz3DqMN8CyXI2x6oUMO5FbxLlM+RoIMgW6DPivvH5j5GKJi4V1d8PTf8Ez9VI
### 2m3hS40CgYEAziP9zsyXe7LfN8/9I4bpTbwXJSSrmd+iJEm5zZa1YpmGbq8pLIAl
### dhyDWqqW6CRglnKj17LNALsQ1R5c9HWrpWiWklsBRhxLgFPZUrHu9pML1Vqhsb+h
### 4Im5Seu8GNDjmvFxwyu2jOxKfZrszEA8JI42x3wthQAlpi0M2a6Bow8CgYEAwiMR
### CAISPIsYLyHPC9qJsNS0mOx66oeQ1qivlny/O2f7FTX5cYanCB7A9E4/wWEXtcGT
### A+moPXJrBzD6vOHj5X3pGaIy5QzIHS7QAxfEz25KG8CnHu2laKK+7GgZEFSJLzBV
### 1nY6Ao7eOXGwmlOUGvySvcK27xiwctTWCg+ilx8CgYAGynq/CFBpO9BMt2ZgeD5R
### WEPu7lFU5WAjD+a2FiPtlp8uIEkb9TDGvlO5des4xJIGAsrQ/UvxnTgFvVVUnFfG
### QDbNpX/aPm5iUODU75Wuu9Nk+PJgt65uhjr7a2+r5Z3J6fBUfKkr5VDi6Gqqo+cX
### UTQOyFF7kzCqU8ublDsjEwKBgA9mlsBK4cV5x1Zd2T34fTnQ9e63u9yFGqFcymkm
### xkjhK9L38CzoYLHNNsNqvbYKjMX3cEE8NQOC4yiz4wA+70RSY76L1nvDMtDV2NyV
### 8x/9gNWIx/k1Iw0U9A5vB6X6/tMGg5i60klHqitmcHsdrD8j0ERLgRWgoMbLSZuM
### t/pDAoGBAK2gZoAaZW8l+99fY0t2uAWAVYVpYG+J0GoFUIaXW7A+K2qmPCvYV/2m
### 6wskhKnSUsADQTvz/4VJZQHAQm1sNfyTEq67qkjJwk1ZfPiPgzNIi3zATyL/gmPD
### SEPrPDzpCrr6P1HD6za1NPknyl9gJ4I9tkRydLd3Tssj31yH/5X4
### -----END RSA PRIVATE KEY-----
### 
### </key>

