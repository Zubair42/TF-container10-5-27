#!/bin/bash



#Configure ip and route
ifconfig eth0 "$MY_IP/16"
#route add default gw 10.10.1.1
#echo "nameserver 10.10.1.1" > /etc/resolv.conf


#SSH Password less Auth
cat > /root/id_rsa <<EOF
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAzHBtAdKvG5YxpCLqnj20avqlptg3VA5+0NCbroWKGl90Ok/v
/ERKky1O3NHxM1gciHnfvx3TB8jbVhok8c/I4yuGpptt7PVdXMwPHJLNCEY80NNA
WFFRhU1VM2NeaFQtLe2w9TuwOH3g/CcuTvZ+JgeHArU6aGXze1P1EN1rtPr1VC96
N8xzdIozZiAhVOV9ntcfmztb5589VRTz0y7jqPduRpQ6nVd6HA/36PIDUzwM+iPc
xMvmN5/6kWSu3x5oZUi6MeCpsCiYtGvdtWf3peZ+Ac6AgSuhiFefQcDNQXR1yxK8
U7zaxDqebTQM2ZJ3ta177r8s8dUviEMtByIkoQIDAQABAoIBABlGFNPS3f8FFGH8
RxEFA9bt4ai8tipYGGlsQLLUo3wbLN1AoKSJNMb5JPS7p5v3Wp+soXKZF0uxExyT
4tlaHLWo3d6e/Hn842vjdkLMCzjopgAS/bNtaiyWoN/XewZe0a8J4YSPunthZH0H
wvvUEUhWSeSVzwC1CeLHQFazaOtYml1EGWOfsUDapIbups0RZ1teffAaN4mXks5e
DcJcZwAo0x2DM/IK1ZE0UOdBJBRwmYjGg1pLmPTbWVUnHfa6UVd80vkaKAYcrh9J
L5YfUILzlGXc236hDeMSTvrQ0/qTM4j1OZlDiIOIwTCa5LcqSxlVK1NvHfI7C6J5
yfFGwPkCgYEA9thc+O1RvmgphD37gZS2ZXnocZVVv9C60HhqdzTQ9IJbmlKk3rH+
fYuRiERjWa1XvwNZn6o/df17XKRvlYiAkZQPhgMngwFPiFgJQP+ozTwfcDuaGaLz
Z85+wwR1n236QGbcsLTWEW2eKYHlI7dRds/1xXl6oczk8kd4jeOlZeMCgYEA1AVx
3Dx6rfQNGQVJKKNskaR2kcGkZ5S9fCgn7GVsfTGcKrU8eSOQ3Nb1jczPhWNDedcw
/Q/3Eh3ShYRLImFJmVXc7PcDbpbwL4RIW+T80sEb8bvNGXimKs7idtcdVorERM5K
UCvJ+hcv7eSrkzAPxE5LsWIrSRBORs+Am6EYcqsCgYAnlvX6Wp2kqzIVapd+5eoH
BnFYXjodhI2hx95Tdh4ZSRsF+kFFEkkdu5fAwi8o/9s14ISsTfLVqqzxY9IdqoHs
V1HSDYl1wm0OEgiR6y8Yk477QiPp/q+JM54zCvEOe+YKFDVLBgXbT7hxMcsdWlZv
dtUVMfGQJ7uEfCACqcMqPwKBgCHI2FLaxEWxzOB+yiCSofs3NLmXHKZwuhztnSb7
JmVwoUzg4kj/VNcL3ccBqtO7dIYWtzSr9ZfKbsBdy0coL30ihHiyB7b4kD9SGeGi
xHKk2NoVLeJwKjJKEyYSsxcWN54rLQZHZADY7wc+V+S1VN3YCBG07ebPlVmNORYT
fnzPAoGAWrpw/5lFTdO+5vQhhPqkxqjUDOXj2S5x6uslLaqLYJNukO2vmaC4bLX6
PV9RnFVA/Jz8p4QtpGjaEj9MbhgFP9wOif0lxbSJubI6PjCOfYE7ge8sG7O9mn5r
C3Vi6hXNJN2q0pZU50Nm6Lu5xewDXaLp1MiYZ1zWkjknG/PqcHc=
-----END RSA PRIVATE KEY-----
EOF

chmod 400 /root/id_rsa

pptpsetup --create p1 --server 90.90.90.2 --username $USERNAME --password $PASSWORD


pon p1
sleep 5

route add -net 192.168.51.0 netmask 255.255.255.0 gw 55.55.55.1 dev ppp0
#
#sleep 5
#
ping 192.168.51.2 -c 2
