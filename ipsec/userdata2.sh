#!/bin/bash

export VPN_SERVER_IP='192.168.20.1'
export VPN_IPSEC_PSK='123456789'
#export VPN_USER='zubair'
#export VPN_PASSWORD='zubair'


#Configure ip and route
ip addr add "$MY_IP/16" dev eth0
#ifconfig eth0 "$MY_IP/16"
#route add default gw 90.90.1.1
#echo "nameserver 10.10.1.1" > /etc/resolv.conf


#sleep 30
#Configure IPSEC

cat > /etc/ipsec.conf <<EOF
# ipsec.conf - strongSwan IPsec configuration file

# basic configuration

config setup
  # strictcrlpolicy=yes
  # uniqueids = no

# Add connections here.

# Sample VPN connections

conn %default
  ikelifetime=60m
  keylife=20m
  rekeymargin=3m
  keyingtries=1
  keyexchange=ikev1
  authby=secret
  ike=aes128-sha1-modp2048!
  esp=aes128-sha1-modp2048!

conn myvpn
  keyexchange=ikev1
  left=%defaultroute
  auto=add
  authby=secret
  type=transport
  leftprotoport=17/1701
  rightprotoport=17/1701
  right=$VPN_SERVER_IP
conn test
  left=%any
  right=$VPN_SERVER_IP
  leftsubnet=90.90.0.0/16
  rightsubnet=192.168.70.0/24
  keyexchange=ikev1
  auto=route
  aggressive=no
  authby=secret
  ike=aes128-sha1-modp1024!
  ikelifetime=3600
  lifetime=1800
  esp=aes128-sha1!
  type=tunnel
conn sss
  left=$MY_IP
  right=$VPN_SERVER_IP
  leftid=%any
  rightid=%any
  leftsubnet=90.90.0.0/16
  rightsubnet=192.168.20.0/24
  keyexchange=ikev1
  auto=route
  aggressive=no
  leftauth=psk
  leftauth2=xauth
  rightauth=psk
  xauth_identity=$USERNAME
  ike=3des-aes128-sha1-modp1024!
  ikelifetime=28800
  lifetime=3600
  esp=3des-aes128-sha1!
  type=tunnel 

EOF

#Configure IPSEC Secret
cat > /etc/ipsec.secrets <<EOF
$MY_IP : PSK 123456789
$VPN_SERVER_IP : PSK 123456789
$MY_IP $VPN_SERVER_IP  $USERNAME : XAUTH $PASSWORD // use xauth1 to xauth2000
EOF

chmod 600 /etc/ipsec.secrets


#Resrart IPSEC and xl2tp services
#service strongswan restart
#service xl2tpd restart
systemctl restart ipsec
sleep 30

#Start Session
ipsec up sss
#echo "c myvpn" > /var/run/xl2tpd/l2tp-control

#sleep 10
#route add $VPN_SERVER_IP gw 10.10.1.1
#route add default dev ppp0

#sleep 10
#wget -qO- http://ipv4.icanhazip.com; echo
