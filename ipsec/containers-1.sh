#!/bin/bash

NO_OF_CONTAINERS=10
LXC_CONTAINER_BASE_NAME="client-"
#LXC_IMAGE_NAME="tf1-vpn-clients-image"
LXC_IMAGE_NAME="ipsec-v5-8-2"
IP_ADDR_START="90.90.90."
IP_ADDR_SUB=10
for i in $(seq $IP_ADDR_SUB $NO_OF_CONTAINERS);
	do
#	IP_ADDR_SUB=`expr $IP_ADDR_SUB + 1` 
	IP_ADDR="$IP_ADDR_START$IP_ADDR_SUB"
	LXC_CONTAINER_NAME="$LXC_CONTAINER_BASE_NAME$i"
	echo $IP_ADDR
	echo $LXC_CONTAINER_NAME
        lxc launch $LXC_IMAGE_NAME  $LXC_CONTAINER_NAME
#	sleep 10
#	cat userdata.sh | lxc exec $LXC_CONTAINER_NAME --env MY_IP=$IP_ADDR bash
	
        IP_ADDR_SUB=`expr $IP_ADDR_SUB + 1`
	done

