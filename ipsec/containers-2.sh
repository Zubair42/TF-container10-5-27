#!/bin/bash

NO_OF_CONTAINERS=1
LXC_CONTAINER_BASE_NAME="ipsec-vpn-client-"
LXC_IMAGE_NAME="tf1-ipsec-client"
IP_ADDR_START="90.90.91."
IP_ADDR_SUB=2
for i in $(seq $IP_ADDR_SUB $NO_OF_CONTAINERS);
	do
	IP_ADDR="$IP_ADDR_START$IP_ADDR_SUB"
	LXC_CONTAINER_NAME="$LXC_CONTAINER_BASE_NAME$i"
	echo $IP_ADDR
	echo $LXC_CONTAINER_NAME
#        lxc launch $LXC_IMAGE_NAME  $LXC_CONTAINER_NAME
#	sleep 10
        lxc exec $LXC_CONTAINER_NAME -- bash -c "killall -9 ping"
        lxc exec $LXC_CONTAINER_NAME -- bash -c "killall -9 ssh"
	cat userdata.sh | lxc exec $LXC_CONTAINER_NAME --env MY_IP=$IP_ADDR bash
        lxc exec $LXC_CONTAINER_NAME -- nohup bash -c "ping 192.168.12.10 &"
	lxc exec $LXC_CONTAINER_NAME -- nohup bash -c "ssh -o ServerAliveInterval=30 -o ServerAliveCountMax=120 -o StrictHostKeyChecking=no -i /root/id_rsa -f -N root@192.168.12.10"
#	cat wget.sh | lxc exec $LXC_CONTAINER_NAME bash
#        lxc exec $LXC_CONTAINER_NAME -- nohup bash -c "sh /root/wget.sh&"
	IP_ADDR_SUB=`expr $IP_ADDR_SUB + 1`
 	done

