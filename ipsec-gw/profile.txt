### Create required bridges


t^P2!k412^socd1c
## Wan Connected Port

ip link add name br1 type bridge
ip link set enp2s0 master br1

## Lan Connected Port

ip link add name br2 type bridge
ip link set enp1s0 master br2

### Edit configuration of Lan connected host

lxc config edit ipsec-lan-host

### Replace with this content in the editor that is open###

devices:
  eth0:
    name: eth0
    nictype: bridged
    parent: br2
    type: nic

###Create required profile for bridging wan gateways

lxc profile create tf1-ipsec-gw-clinet

lxc profile edit tf1-ipsec-gw-clinet

### Replace with this content in the editor that is open###

config: {}
description: ""
devices:
  eth0:
    name: eth0
    nictype: bridged
    parent: br1
    type: nic
name: tf1-ipsec-gw-clinet

