#!/bin/bash



#Configure ip and route
ip addr flush dev eth0

#route add default gw 10.10.21.1

sleep 5

ifconfig eth0 "$MY_IP/16"

route add default gw 2.2.2.2
#echo "nameserver 10.10.1.1" > /etc/resolv.conf

#Generate IPSec Secrets file

cat > /etc/ipsec.secrets << EOF

# This file holds shared secrets or RSA private keys for authentication.

# RSA private key for this host, authenticating it to any other host
# which knows the public part.
$MY_IP 2.2.2.2 : RSA server.key

EOF

#Generate IPSec Configuration file /etc/ipsec.conf

cat > /etc/ipsec.conf <<EOF

config setup
	# strictcrlpolicy=yes

conn Host
 left=$MY_IP
 right=2.2.2.2
 leftsubnet=$LAN_NET/24
 rightsubnet=192.168.10.0/24
 keyexchange=ikev1
 auto=route
 aggressive=yes
 leftcert=server.crt
 leftid="CN=Default "
 rightid=%any
 ike=blowfish-sha256-modp8192!
 ikelifetime=28800
 lifetime=14400
 ah=3des-aes128-sha256!
 type=tunnel


EOF

ip link add eth10 type dummy
sleep 5
ip link set eth10 up

sleep 5

ip addr add $IP_LAN dev eth10
#ifconfig eth10 $IP_LAN

mkdir -p /etc/ipsec.d/certs
mkdir -p /etc/ipsec.d/cacerts
mkdir -p /etc/ipsec.d/private

cat > /etc/ipsec.d/private/server.key <<EOF
-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDOc82T4oEJcbTF
d11iBJ8SfG6ueVPU/fX5u4YTBaeQzY8fLYjISpPWyY8lXXQfyeBfx8/lIce7W5Em
V+mTG5Lsqwql9Q5RsLN2kXDxiHhRIuiGdWepjD4DUW5OSO1UFd2VWraZWDA4SXnn
5bWV5Vme1auLD5M90cphX4bgEkTo8BVp4ufJqsOYl6nHeHJEWlHQXDg8f23jdm21
Y4MT7hwb/ZZbRq7yIP4YmMNGG5y0ibFnHfIlhG9yv8cf8dbu4RohlPVzL70m9oQx
ahw63yggOu1vFvGP1H8VRb3NpeKAB5IYDp+vmODcc7hhEY22hpIWbPip0rRVg87C
Zb3XNNC/AgMBAAECggEAPwtT8t4PQ4AKkOaDiMQag32mGveH5xnqfGmMZtCqO+jZ
1M6F34wOm1+Pq/xe0rpGH1mYHcN0B73NlGTyOe8IrecJaGZuCfoow4Nkdw2SmExN
r4m4ryjZYxAB3Dwe5TeMI+jArBEnIHknle4qxmx0MrHTIbMO5qji4VtXh6Nskaj6
fy7SK8iWZLJkpoBN2AIarNQkBK/EpIcDoQy6uYH4aTCFnRrUEU4wdHQqpI8L0uFf
cumvvL9S/aoHqFPSm6TnfXUWJwBXWDWA/Rf+R391VpjMQuDmVZSIBP7KWgWCDTHF
Dl4kPAEaVKNXzh6syEt4sOa8la3RbYje4iN2o/fcMQKBgQDxMiJFfWvMNjVyYmsu
Nr6OZqylfVBLbTeOOgWu/4WhlrpAHINkZpskgnfNIBbwYp5o90ScuGLMJEe8w5+Y
RBIQCMYUKtUBxhVNRQD21vkfcsKZ/5lT0kF8n2JX4nPCLqnc1hM/NwA+i3VvAQfE
rp4FOhqCfZe3IEqWUpG9xQ2SJwKBgQDbH8BPRaYG1UF3GUL8iywb17WgKuFLAfyB
+U8R2QUtgzecBAsZ/G6IpS02962Ojf/FYed/oiBmhUSqOf66jHkdsGusNXRtD4oT
NtiEu7D2mi2s6uCR5u52Zck1h7q9keB2IQctBWPWpoXPEWOIcjD20nrWuBbtjyUu
7rebBhwjqQKBgQCC61iI6ZQ6q24FYYCV/Ccg+oBZ75RG7MiMDdiJ7a6mUD9Wj2OO
etdZUOwgTnvFDXK4vQbkHn2aG1NJFdIp/0DirTB/P+SQ6ukjIQJt+bANrugMWR3r
zqO3NnhWBdgZneSbtyOJW5xpVp9umluSGcoyQiCjlfJk90UlJTpwjGZaXwKBgQDI
5SpWQ1+9QgVcIO/2Q85z1GezabzWHQMZUq44icSifnwccOSQtYvNgcqba6ZWXT/T
AjjRHp4gACz8+GDfPWzJWHbPEs8kMiwAA7essbvDy4T21PyBJ70XFajMiB0gMj8d
bph6oUdwg4mIV8PuU+sqT7zJYUu8DxIjHj99RLPFyQKBgCt+8+gY/AkFLGglT9uR
P/uc7/Dp3b3x/7cYDXMm1M9DMS2fwFvQ3EePoQzmw1Tp4vUAKxD8wrQ+MdLHbxQ6
Zq7iWUMDi4ETviJsaevEsXH7jxthZm0uMei3Zsk0tQvx/piaSsYiKOzh9y5LIuZ5
BUHFbNyMhN3F8JL/hfk4fyI1
-----END PRIVATE KEY-----
EOF

cat > /etc/ipsec.d/cacerts/ca.crt <<EOF
-----BEGIN CERTIFICATE-----
MIIDKDCCAhCgAwIBAgIJAJqX8wxapEM/MA0GCSqGSIb3DQEBCwUAMBIxEDAOBgNV
BAMMB051Y2xpYXMwHhcNMjEwNjA5MTYxMDA1WhcNMzEwNjA3MTYxMDA1WjASMRAw
DgYDVQQDDAdOdWNsaWFzMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA
oB+KJMNa3LLTAiBKpZ1ADK4RAklyqTkVAtoyjbmaWhQ5KEBw9FQAgK59Szb8CqS8
zE0WNBLXyDAgPwoMz31L/18taqd5J/El0B6IAgngxLrHKwwZPzmzgwy98aj/FH68
Vs4B0QeRFm9SBlin2s1ltaSFKQLD5R4oDz/p09xU9EL58ZhDOdc5ySp8+LIUL6s7
GGLY5jNLFrys+houyQwQEyQyBUtX3rIVQuMhEfwTAMiQFgE3KLs6YECkf3CZvSpO
sH2MCXWw8ov2J/V0EEsP7gMsss/99YHdVZXA7RqU2WofLdq+VSoz9FtgLgORSSmD
DKE/vfL5BlSIcZoe+YeI+QIDAQABo4GAMH4wHQYDVR0OBBYEFJbI6SgJYRBB7BHK
yufz5sPVD6uVMEIGA1UdIwQ7MDmAFJbI6SgJYRBB7BHKyufz5sPVD6uVoRakFDAS
MRAwDgYDVQQDDAdOdWNsaWFzggkAmpfzDFqkQz8wDAYDVR0TBAUwAwEB/zALBgNV
HQ8EBAMCAQYwDQYJKoZIhvcNAQELBQADggEBACJfdzptXKatc7kqsYWDNkgMSKxN
ij0HjpbFHnvz/Dmycc/KT+3HKcH0bSCgG0H8Fv3nzQJZzT1Ku3LgLzjqzR51GIqd
GVUUw4zN71DJgJ7TLN2RoB8XCoT1uRbqnM/mrUl85Tv9huC+gu/QfZC8T2Ot+JV9
+2iAQYy1yWdnb+CIuisrqNLW44rcrLK0J6A85pulSMAvxj20M+xhwKseNw/Tvd3H
kjj0ObNmFCNyEWtqAclvb8Oopm1NegRgcHWa+yAdwlJjK0flHA6S1DeYuiG5Fxai
o9bZ+MYnUo6tTrm7UqwazD+BSD+euHJ1tGm1kz5m+2ZrsC1+3aHoL+RI0rA=
-----END CERTIFICATE-----
EOF

cat > /etc/ipsec.d/certs/server.crt <<EOF
-----BEGIN CERTIFICATE-----
MIIDQjCCAiqgAwIBAgIQFg+cw20qNoKRS/skdZ2TvDANBgkqhkiG9w0BAQsFADAS
MRAwDgYDVQQDDAdOdWNsaWFzMB4XDTIxMDYwOTE2MTAxMFoXDTIzMDYwOTE2MTAx
MFowEjEQMA4GA1UEAwwHRGVmYXVsdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCC
AQoCggEBAM5zzZPigQlxtMV3XWIEnxJ8bq55U9T99fm7hhMFp5DNjx8tiMhKk9bJ
jyVddB/J4F/Hz+Uhx7tbkSZX6ZMbkuyrCqX1DlGws3aRcPGIeFEi6IZ1Z6mMPgNR
bk5I7VQV3ZVatplYMDhJeefltZXlWZ7Vq4sPkz3RymFfhuASROjwFWni58mqw5iX
qcd4ckRaUdBcODx/beN2bbVjgxPuHBv9lltGrvIg/hiYw0YbnLSJsWcd8iWEb3K/
xx/x1u7hGiGU9XMvvSb2hDFqHDrfKCA67W8W8Y/UfxVFvc2l4oAHkhgOn6+Y4Nxz
uGERjbaGkhZs+KnStFWDzsJlvdc00L8CAwEAAaOBkzCBkDAJBgNVHRMEAjAAMB0G
A1UdDgQWBBR6F0konVg84oDIHunYcOgXxOgZOjBCBgNVHSMEOzA5gBSWyOkoCWEQ
QewRysrn8+bD1Q+rlaEWpBQwEjEQMA4GA1UEAwwHTnVjbGlhc4IJAJqX8wxapEM/
MBMGA1UdJQQMMAoGCCsGAQUFBwMCMAsGA1UdDwQEAwIHgDANBgkqhkiG9w0BAQsF
AAOCAQEAKSVOU9EyY5o7ChMysOU9RanW/DKSB05hGjiY8CGWDHBIX1U9S9SUR1y3
hLcLUd/thlG7evDSq1RNMxW58/T/HIazEHhaIq67wePy/xm3+TnVTnCpmn/Brbp/
SAONSSDXeYiEDONfoAKwKjZiJbMZk29ODK09XoOfe/PYsvllOfTAh/w2lc5bqA6B
nl14aD0aJEMmXH6sfCdqH9bCuLXBe6MMnNPjTR5MMFW59SUb88w3VCX5ismE4PUa
O/MBEGRCY7MCYvpuz3fOxzmc7EssFM2G8EXfs23vYEsXr6Od1HRLCuBAI1x6LI8a
hJF7yqcK0h3+7uRMzpTOuTEPREGwcg==
-----END CERTIFICATE-----
EOF

sleep 2

ip a
#route add default gw 90.90.90.2

sleep 2

systemctl restart strongswan


#ipsec up Host

sleep 2

systemctl status strongswan

sleep 2

ipsec status

ping 192.168.10.100 -c 4


