#!/bin/bash



#Configure ip and route
ip addr flush dev eth0

#route add default gw 10.10.21.1

sleep 5

ifconfig eth0 "$MY_IP/16"

route add default gw 11.11.1.3
#echo "nameserver 10.10.1.1" > /etc/resolv.conf

#Generate IPSec Secrets file

cat > /etc/ipsec.secrets << EOF

# This file holds shared secrets or RSA private keys for authentication.

# RSA private key for this host, authenticating it to any other host
# which knows the public part.

#$MY_IP : PSK 123456789
#11.11.1.3 PSK 123456789
$MY_IP 11.11.1.3 : PSK "1234567890"
EOF

#Generate IPSec Configuration file /etc/ipsec.conf

cat > /etc/ipsec.conf <<EOF
config setup
 plutodebug=all
 plutostderrlog=/var/log/openswan.log
 protostack=klips

conn Ipsec$CON_NO
 left=$MY_IP
 right=11.11.1.3
 leftid=%any
 rightid=%any
 leftsubnet=$LAN_NET/24
 rightsubnet=192.168.10.0/24
 auto=route
 aggrmode=no
 pfs=yes
 authby=secret
 keyexchange=ikev1 
 ike=3des-aes128-sha1-modp1024!
 ikelifetime=50000s
 salifetime=36000s
 esp=3des-aes128-sha1-modp1024!
 type=tunnel
 leftupdown="/usr/lib/ipsec/_updown"
EOF

#
ip link add eth10 type dummy
sleep 5
ip link set eth10 up

sleep 5

ip addr add $IP_LAN dev eth10
#ifconfig eth10 $IP_LAN


ip a
#route add default gw 90.90.90.2

sleep 2

systemctl restart strongswan


#ipsec up Host

sleep 2

systemctl status strongswan

sleep 2

ipsec status

ping 192.168.10.9 -c 4
