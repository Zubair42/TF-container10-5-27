#!/bin/bash



#Configure ip and route
ip addr flush dev eth0

#route add default gw 10.10.21.1

sleep 5

ifconfig eth0 "$MY_IP/16"

route add default gw 2.2.2.2
#echo "nameserver 10.10.1.1" > /etc/resolv.conf

#Generate IPSec Secrets file

cat > /etc/ipsec.secrets << EOF

# This file holds shared secrets or RSA private keys for authentication.

# RSA private key for this host, authenticating it to any other host
# which knows the public part.

$MY_IP : PSK 123456789superHero
2.2.2.2 : PSK 123456789superHero

EOF

#Generate IPSec Configuration file /etc/ipsec.conf

cat > /etc/ipsec.conf <<EOF

config setup
	# strictcrlpolicy=yes
	# uniqueids = no

conn Host
 left=$MY_IP
 right=2.2.2.2
 leftid=%any
 rightid=%any
 leftsubnet=$LAN_NET/24
 rightsubnet=192.168.10.100/24
 keyexchange=ikev1
 auto=route
 aggressive=no
 authby=secret
 ike=des-cast128-sha512-modp1024!
 ikelifetime=28800
 lifetime=14400
 ah=3des-aes128-sha256!
 type=tunnel



EOF

ip link add eth10 type dummy
sleep 5
ip link set eth10 up

sleep 5

ip addr add $IP_LAN/24 dev eth10
#ifconfig eth10 $IP_LAN

#SSH Password less Auth
cat > /root/id_rsa <<EOF
-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAzHBtAdKvG5YxpCLqnj20avqlptg3VA5+0NCbroWKGl90Ok/v
/ERKky1O3NHxM1gciHnfvx3TB8jbVhok8c/I4yuGpptt7PVdXMwPHJLNCEY80NNA
WFFRhU1VM2NeaFQtLe2w9TuwOH3g/CcuTvZ+JgeHArU6aGXze1P1EN1rtPr1VC96
N8xzdIozZiAhVOV9ntcfmztb5589VRTz0y7jqPduRpQ6nVd6HA/36PIDUzwM+iPc
xMvmN5/6kWSu3x5oZUi6MeCpsCiYtGvdtWf3peZ+Ac6AgSuhiFefQcDNQXR1yxK8
U7zaxDqebTQM2ZJ3ta177r8s8dUviEMtByIkoQIDAQABAoIBABlGFNPS3f8FFGH8
RxEFA9bt4ai8tipYGGlsQLLUo3wbLN1AoKSJNMb5JPS7p5v3Wp+soXKZF0uxExyT
4tlaHLWo3d6e/Hn842vjdkLMCzjopgAS/bNtaiyWoN/XewZe0a8J4YSPunthZH0H
wvvUEUhWSeSVzwC1CeLHQFazaOtYml1EGWOfsUDapIbups0RZ1teffAaN4mXks5e
DcJcZwAo0x2DM/IK1ZE0UOdBJBRwmYjGg1pLmPTbWVUnHfa6UVd80vkaKAYcrh9J
L5YfUILzlGXc236hDeMSTvrQ0/qTM4j1OZlDiIOIwTCa5LcqSxlVK1NvHfI7C6J5
yfFGwPkCgYEA9thc+O1RvmgphD37gZS2ZXnocZVVv9C60HhqdzTQ9IJbmlKk3rH+
fYuRiERjWa1XvwNZn6o/df17XKRvlYiAkZQPhgMngwFPiFgJQP+ozTwfcDuaGaLz
Z85+wwR1n236QGbcsLTWEW2eKYHlI7dRds/1xXl6oczk8kd4jeOlZeMCgYEA1AVx
3Dx6rfQNGQVJKKNskaR2kcGkZ5S9fCgn7GVsfTGcKrU8eSOQ3Nb1jczPhWNDedcw
/Q/3Eh3ShYRLImFJmVXc7PcDbpbwL4RIW+T80sEb8bvNGXimKs7idtcdVorERM5K
UCvJ+hcv7eSrkzAPxE5LsWIrSRBORs+Am6EYcqsCgYAnlvX6Wp2kqzIVapd+5eoH
BnFYXjodhI2hx95Tdh4ZSRsF+kFFEkkdu5fAwi8o/9s14ISsTfLVqqzxY9IdqoHs
V1HSDYl1wm0OEgiR6y8Yk477QiPp/q+JM54zCvEOe+YKFDVLBgXbT7hxMcsdWlZv
dtUVMfGQJ7uEfCACqcMqPwKBgCHI2FLaxEWxzOB+yiCSofs3NLmXHKZwuhztnSb7
JmVwoUzg4kj/VNcL3ccBqtO7dIYWtzSr9ZfKbsBdy0coL30ihHiyB7b4kD9SGeGi
xHKk2NoVLeJwKjJKEyYSsxcWN54rLQZHZADY7wc+V+S1VN3YCBG07ebPlVmNORYT
fnzPAoGAWrpw/5lFTdO+5vQhhPqkxqjUDOXj2S5x6uslLaqLYJNukO2vmaC4bLX6
PV9RnFVA/Jz8p4QtpGjaEj9MbhgFP9wOif0lxbSJubI6PjCOfYE7ge8sG7O9mn5r
C3Vi6hXNJN2q0pZU50Nm6Lu5xewDXaLp1MiYZ1zWkjknG/PqcHc=
-----END RSA PRIVATE KEY-----
EOF

chmod 400 /root/id_rsa


sleep 2

ip a
#route add default gw 90.90.90.2

sleep 2

systemctl restart strongswan


#ipsec up Host

sleep 2

systemctl status strongswan

sleep 2

ipsec status

ping 192.168.10.100 -c 4
