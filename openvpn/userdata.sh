#!/bin/bash



#Configure ip and route
ifconfig eth0 "$MY_IP/24"
route add default gw 2.2.2.1
#echo "nameserver 10.10.1.1" > /etc/resolv.conf

#create login.conf

cat > /root/login.conf <<EOF
$USERNAME
$PASSWORD
EOF




cat > /root/openvpn.sh <<EOF
#sed 's/### //' client_config.ovpn > openvpn_client_config.ovpn
sudo openvpn --config client_config.ovpn --auth-user-pass login.conf --daemon

EOF
cat > /root/client_config.ovpn << EOF
remote 2.2.2.200
dev tun1
client
port 1194
proto udp
cipher AES-128-CBC
auth SHA1
auth-user-pass
<ca>
-----BEGIN CERTIFICATE-----
MIICZDCCAc2gAwIBAgIUMb3urXqJaltQGRhQsI5uRM+/et0wDQYJKoZIhvcNAQEL
BQAwIDEeMBwGA1UEAwwVRC1MaW5rIENvcnBvcmF0aW9uIENBMB4XDTIwMDMzMDE0
NDUzNVoXDTMwMDMyODE0NDUzNVowIDEeMBwGA1UEAwwVRC1MaW5rIENvcnBvcmF0
aW9uIENBMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDdhc0l+NA3/jCS97yl
yOsfB3E46KLSSveoM0LLeMiPpoNGjjioicFxBhsJz3bpm2iVDU/Zgk7rc29htRS+
4iXf6sfYhTxZ2+KBpR2MtcrhpnWiMs5a4+6O4TeQ+7rKr12Rj44axOIcV7Cx8k0P
xUnmg5sDYqt4FYCKOIy5q1UTywIDAQABo4GaMIGXMB0GA1UdDgQWBBSkrOjkMDKJ
T6XYPV+uFog6X22KIzBbBgNVHSMEVDBSgBSkrOjkMDKJT6XYPV+uFog6X22KI6Ek
pCIwIDEeMBwGA1UEAwwVRC1MaW5rIENvcnBvcmF0aW9uIENBghQxve6teolqW1AZ
GFCwjm5Ez7963TAMBgNVHRMEBTADAQH/MAsGA1UdDwQEAwIBBjANBgkqhkiG9w0B
AQsFAAOBgQCam4tRGkxvzEnEirZ0+H4RSZ4CYoGWwjdz8FvanLHp8X0CjMpEMJ4E
sHDltMhCZvicy5Xb85XgC2mDfPNi7O6Rgh/2KQZeH91FF9YYvzSgaSLZnccCHPM4
t8jooQ0w4zPMmS2nJEP/udUYzzg0NYsnlS1iZXhtv2UUHzmSbRpFyQ==
-----END CERTIFICATE-----

</ca>
<cert>
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            42:51:db:07:e7:2e:8d:67:98:62:e9:70:6e:2a:ba:5d
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: CN=D-Link Corporation CA
        Validity
            Not Before: Jan  1 00:07:07 2000 GMT
            Not After : Dec 30 00:07:07 2036 GMT
        Subject: CN=client
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:c4:3f:cd:bc:e2:2c:76:ed:cd:1a:9e:2e:cd:81:
                    6c:19:6b:c6:f4:1b:5c:d9:c1:2b:55:ab:7d:54:a2:
                    4b:cb:ab:aa:af:ff:00:a3:b7:22:37:ba:3a:0d:72:
                    08:1f:1f:fc:69:0f:1f:4d:a4:18:8a:58:8a:2d:54:
                    65:18:9b:a2:f4:41:e5:4e:39:8d:1b:b5:ae:f9:2d:
                    2e:a4:d1:d5:cc:89:98:16:78:b1:7b:de:e2:cd:e6:
                    bc:7c:60:b6:cb:e6:20:40:f2:ba:10:ce:50:8c:0c:
                    9b:a3:bd:b3:09:8c:6a:34:1c:a1:8b:39:ef:8c:b0:
                    df:0a:63:14:88:6a:79:2e:86:89:f1:04:28:db:c0:
                    8f:5e:3d:88:cf:47:78:86:85:73:8d:73:75:b5:17:
                    14:75:54:70:61:8d:0a:31:2e:ee:16:27:b8:de:b7:
                    ba:65:1e:0b:1e:bd:76:a9:fc:0c:64:18:c6:c3:d0:
                    af:a9:2d:46:f7:90:0f:01:0f:75:61:cf:c1:32:1c:
                    81:e3:67:7b:ab:ff:27:36:7c:46:3d:3f:bd:44:e0:
                    43:07:53:aa:6d:fc:04:8a:a8:f8:9d:90:54:e0:20:
                    e5:6c:04:13:14:78:57:e8:14:a6:d4:5b:1c:16:58:
                    6a:87:3d:a9:62:f1:97:ed:cb:58:29:28:a4:fd:a7:
                    51:8d
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            X509v3 Subject Key Identifier: 
                F7:41:A2:5B:BF:09:CA:5F:FB:79:23:C3:0E:A3:99:48:21:B9:72:20
            X509v3 Authority Key Identifier: 
                keyid:A4:AC:E8:E4:30:32:89:4F:A5:D8:3D:5F:AE:16:88:3A:5F:6D:8A:23
                DirName:/CN=D-Link Corporation CA
                serial:31:BD:EE:AD:7A:89:6A:5B:50:19:18:50:B0:8E:6E:44:CF:BF:7A:DD

            X509v3 Extended Key Usage: 
                TLS Web Client Authentication
            X509v3 Key Usage: 
                Digital Signature
    Signature Algorithm: sha256WithRSAEncryption
         c9:bc:6a:3a:3e:20:58:b1:98:54:dd:3d:25:2d:f0:23:6d:38:
         00:13:e4:9c:79:f2:0b:b7:87:b3:45:ad:14:50:c1:d5:f2:ee:
         a5:63:24:2b:90:b2:20:b6:2e:af:de:aa:75:e7:2e:78:bb:25:
         16:96:af:c8:3c:1d:78:7d:e7:31:ee:0d:84:bf:b0:bb:05:00:
         ce:fe:d6:4a:5c:d4:6d:04:fa:98:f1:ca:7b:44:c6:e1:da:d3:
         72:00:3f:61:f7:4c:14:fe:26:41:12:d1:6a:80:15:c4:d4:d7:
         30:61:0d:71:77:fb:8d:69:98:3e:1a:99:34:c6:c5:cb:bc:77:
         e0:e5
-----BEGIN CERTIFICATE-----
MIIC5zCCAlCgAwIBAgIQQlHbB+cujWeYYulwbiq6XTANBgkqhkiG9w0BAQsFADAg
MR4wHAYDVQQDDBVELUxpbmsgQ29ycG9yYXRpb24gQ0EwHhcNMDAwMTAxMDAwNzA3
WhcNMzYxMjMwMDAwNzA3WjARMQ8wDQYDVQQDDAZjbGllbnQwggEiMA0GCSqGSIb3
DQEBAQUAA4IBDwAwggEKAoIBAQDEP8284ix27c0ani7NgWwZa8b0G1zZwStVq31U
okvLq6qv/wCjtyI3ujoNcggfH/xpDx9NpBiKWIotVGUYm6L0QeVOOY0bta75LS6k
0dXMiZgWeLF73uLN5rx8YLbL5iBA8roQzlCMDJujvbMJjGo0HKGLOe+MsN8KYxSI
ankuhonxBCjbwI9ePYjPR3iGhXONc3W1FxR1VHBhjQoxLu4WJ7jet7plHgsevXap
/AxkGMbD0K+pLUb3kA8BD3Vhz8EyHIHjZ3ur/yc2fEY9P71E4EMHU6pt/ASKqPid
kFTgIOVsBBMUeFfoFKbUWxwWWGqHPali8Zfty1gpKKT9p1GNAgMBAAGjgawwgakw
CQYDVR0TBAIwADAdBgNVHQ4EFgQU90GiW78Jyl/7eSPDDqOZSCG5ciAwWwYDVR0j
BFQwUoAUpKzo5DAyiU+l2D1frhaIOl9tiiOhJKQiMCAxHjAcBgNVBAMMFUQtTGlu
ayBDb3Jwb3JhdGlvbiBDQYIUMb3urXqJaltQGRhQsI5uRM+/et0wEwYDVR0lBAww
CgYIKwYBBQUHAwIwCwYDVR0PBAQDAgeAMA0GCSqGSIb3DQEBCwUAA4GBAMm8ajo+
IFixmFTdPSUt8CNtOAAT5Jx58gu3h7NFrRRQwdXy7qVjJCuQsiC2Lq/eqnXnLni7
JRaWr8g8HXh95zHuDYS/sLsFAM7+1kpc1G0E+pjxyntExuHa03IAP2H3TBT+JkES
0WqAFcTU1zBhDXF3+41pmD4amTTGxcu8d+Dl
-----END CERTIFICATE-----

</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDEP8284ix27c0a
ni7NgWwZa8b0G1zZwStVq31UokvLq6qv/wCjtyI3ujoNcggfH/xpDx9NpBiKWIot
VGUYm6L0QeVOOY0bta75LS6k0dXMiZgWeLF73uLN5rx8YLbL5iBA8roQzlCMDJuj
vbMJjGo0HKGLOe+MsN8KYxSIankuhonxBCjbwI9ePYjPR3iGhXONc3W1FxR1VHBh
jQoxLu4WJ7jet7plHgsevXap/AxkGMbD0K+pLUb3kA8BD3Vhz8EyHIHjZ3ur/yc2
fEY9P71E4EMHU6pt/ASKqPidkFTgIOVsBBMUeFfoFKbUWxwWWGqHPali8Zfty1gp
KKT9p1GNAgMBAAECggEAUofmlN2lB2rgS6ZkVmZsOJCbvcLYHHdhKwkQjwwlxgRu
ps8dzLvSdw8pJMhk4/oGo4QJjRRKgbvUeFW6okupjwE7VtdbIWEZsY6vw/6+4+Kl
hCXDti04wpCbic7+cOdZfxbU6wZo10ziE01OwuA7Nakowq1dsZQcaHjchLYKOmb7
g2wbwNxt2UEJ4Wx+t26O1KsZwzduER3113kVfJxStehVkNkaFzQ43qhuv71z4wsU
abZMdtU0oZFJDMhEHI23viXPFVV2TehVRvswPL6TGRAABSU4xC0zZ4DruSQh7jvT
QRVdDQn/Mgi8RIFDobhEJ9svIz0EdSisJ+IudoFD3QKBgQDkQhGyUUEnx1+M26iV
JPsoSzXjFG25361c3aQkxstCBF0wif28BgtU2e5eh6fAJOH+ICsGISLPNWyuxH+1
/uyy4OJAOUsWJOPAkPTKLjv6qrdAagZEutsaOE2poEsiSwJ2F3EdgEG7gq9lnhZA
dn5sJsIK6dcUan2zoZklsiOZVwKBgQDcGdLvsOdkV19ANVxv434t6hDHmM07jlLi
wYe/jJwiRcwGCicA/0csNLDM/RcBrwKy7xmlXBJ/f23WA5LemOas+aaMgCGVOuE5
txunC+B+RSkL7fFXAsvWL9/yL/7+mciyltK21z0VM6zF9fgYuWe1kpTy5GcZob2O
h3usIifJuwKBgFvZyk07LUDYlbaU3a3Gh9qbCUArhEv7mkN4XQj/heM25lkV7iiv
NfgbCjS32eUT8mzUfPumcd933BgnskyGHLGOlPzLEVAD4DIpIpgJKtGfyMX+5C7c
Ejbm2Tmc6x8NmBv1Kut4JPtTh0kY6FNgW1ch8Yt6tbXddF/Ic1wb5v9ZAoGAfkNi
OpalZ26n+dGiwZ0j2XsZPSdvNOckrWiQU1Z6iz63GA/i6DOJxNZFRpM6C4E2byey
8qWrBvE3NTq/mmrkif59iMRIf69TXSSp/F5X3pgWP/20IVI9VRYJxom6IpxeAfV7
0+AN7ofCaZ39QVG7KPJ5+A0HCDE86PLYA5tkb9kCgYEAwowi+UbYg86xhxqlFzhC
gqu3LdU1G2vOObKYEbxNbI52O2CePnrzGPN6m8imUrgM+Ze8ccYRijd5yWbPC6h0
0sicmuDj9QhRB7zH7DJOytpdWFGFyHcqgIjU2CqvurGm3MQFj1vZNi/qPW0a5ZX3
hF8eywEScEcRUPNdH5FKep4=
-----END PRIVATE KEY-----

</key>

EOF


#openvpn --remote 90.90.90.2 --port 1194 --dev tun1 --client --proto udp --cipher AES-128-CBC --auth SHA1 --ca ca.crt --cert serverB.crt --key serverB.key --verb 3 --auth-retry nointeract --reneg-sec 0 --auth-user-pass login.conf --daemon

route add default gw 192.168.10.1

sleep 5

#openvpn --remote 90.90.90.2 --port 1194 --dev tun1 --client --proto udp --cipher AES-128-CBC --auth SHA1 --ca ca.crt --cert serverB.crt --key serverB.key --verb 3 --auth-retry nointeract --reneg-sec 0 --auth-user-pass login.conf --daemon

sh /root/openvpn.sh
sleep 5

ping 192.168.10.100 -c 2
