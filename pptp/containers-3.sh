#!/bin/bash

NO_OF_CONTAINERS=85
LXC_CONTAINER_BASE_NAME="client-"
IP_ADDR_START="90.90.94."
IP_ADDR_SUB=21
USER_NAME_BASE="maxmax"
PASSWORD_BASE="maxmax"
for i in $(seq $IP_ADDR_SUB $NO_OF_CONTAINERS);
	do
	PPTP_USER_NAME="$USER_NAME_BASE$i"
	PPTP_PASSWORD="$PASSWORD_BASE$i"
	IP_ADDR="$IP_ADDR_START$IP_ADDR_SUB"
	LXC_CONTAINER_NAME="$LXC_CONTAINER_BASE_NAME$i"
	echo $IP_ADDR
	echo $LXC_CONTAINER_NAME
	lxc exec $LXC_CONTAINER_NAME -- bash -c "pkill pptp"
	lxc exec $LXC_CONTAINER_NAME -- bash -c "pkill ping"
	lxc exec $LXC_CONTAINER_NAME -- bash -c "pkill ssh"
	cat userdata.sh | lxc exec $LXC_CONTAINER_NAME --env MY_IP=$IP_ADDR --env USERNAME=$PPTP_USER_NAME --env PASSWORD=$PPTP_PASSWORD bash
	lxc exec $LXC_CONTAINER_NAME -- nohup bash -c "ping 192.168.51.2 &"
#        lxc exec $LXC_CONTAINER_NAME -- nohup bash -c "ssh -o ServerAliveInterval=30 -o ServerAliveCountMax=120 -o StrictHostKeyChecking=no -i /root/id_rsa -f -N root@192.168.12.10"
	IP_ADDR_SUB=`expr $IP_ADDR_SUB + 1`
	sleep 2
 	done

